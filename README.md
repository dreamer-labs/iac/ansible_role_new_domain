# new_domain


This role will create a brand new Primary Domain Controller with a Active Directory Domain/Forest.  No hardening is applied.

Works on

- Windows Server 2019
- Windows Server 2016
- Windows Server 2012R2

## Requirements

- `python3-winrm` (`pywinrm`) is needed for WinRM.

## Role Variables

### `defaults/main.yml`

| Variable                         | Default value                       | Explanation |
|:---------------------------------|:------------------------------------|:------------|
| new_domain_domainadmin_username      | Administrator                       | Domain admin account used to add a new DC to an existing Domain  |
| new_domain_localadmin_username       | Administrator                       | Local administrator account, generally the Built-in Administrator account   |
| new_domain_domainadmin_upn           | "{{new_domain_domainadmin_username}}@{{new_domain_domain}}" | User Principle Name of the Domain Admin account that will be used to add an additional DC to the domain.  Change the referenced variables instead of this directly |
| new_domain_domain_functional_level      | Default | Specifies the domain functional level of the first domain in the creation of a new forest. The domain functional level cannot be lower than the forest functional level, but it can be higher. Change this depending on your needs. |
| new_domain_forest_functional_level      | Default | Specifies the forest functional level for the new forest. The default forest functional level in Windows Server is typically the same as the version you are running. Change this depending on your needs. |
| new_domain_required_features            | ["AD-domain-services", "DNS"]       | Windows Features that should be installed on the Domain Controller. Defaults to AD-domain-services and DNS. 9/10 times you should leave this to the default value. |
| new_domain_extra_features               | ["DHCP", "NPAS"]              | Additional Windows Features that should be installed on the Domain Controller. |
| new_domain_pdc                          | True                      | With this value set to true, the target host will be treated as a new domain/first domain controller.  Set to false to add target as an additional DC to an existing domain |
| new_domain_ethnames                     | ["Ethernet", "Ethernet 2", "Ethernet 3", "Ethernet 4"]                                   | A list of the ethernet adapters to setup DNS on. |

### Additional required roles

| Variable                         | Example value                       | Explanation |
|:---------------------------------|:------------------------------------|:------------|
| new_domainadmin_password         | P@ssw0rd!                           | The password of the domain admin account. Recommend using ansible vault to obscure this |
| new_domain_localadmin_password          | P@ssw0rd!                           | The password of Built-in Administrator account. This password (if new_domain_localadmin_username left to the default value) will become the password of NETBIOS\Administrator. Change this to a strong password. Recommend using ansible vault to obscure this |
| new_domain_dns_ips                       | ["192.168.1.10", "192.168.1.11"]                       | List of IP addresses to set as DNS servers.  If adding additional DCs, one of these must be an existing DC's IP |
| new_domain_static_ips                    | 192.168.1.9                       | List of static IPs to assign (first interface is first IP in list) |
| new_domain_gws                          | 192.168.1.254                      | List of gateways to assign (first interface is the first gw in list) |
| new_domain_newhostname                   | dc1                                | New hostname of the DC |
| new_domain_domain                       | ad.example.test                     | The Domain of the new Active Directory Forest |
| new_domain_netbios                      | TEST                                | The NetBIOS of the new Active Directory Forest.  |
| new_domain_domain_safe_mode_password    | P@ssw0rd!                           | The Domain Safe Mode password. Recommend using ansible vault to obscure this |

## Dependencies

-

## Example Playbook

    ---
    - name: Build domain
      hosts: domaincontrollers
      roles:
        - role: "ansible_role_new_domain"
          new_domain_domainadmin_password: P@ssw0rd!
          new_domain_localadmin_password: P@ssw0rd!
          new_domain_domain_safe_mode_password: P@ssw0rd!
          new_domain_static_ips: "{{ ipv4_addresses }}"
          new_domain_gws: "{{ ansible_interfaces | selectattr('default_gateway', 'defined') | map(attribute='default_gateway') }}"
          new_domain_dns_ips:
            - "{{ groups['all'] | map('extract', hostvars, ['ansible_host']) | join(',') }}"
          new_domain_domain: test.net
          new_domain_netbios: TEST

## Local Development

This role includes a Vagrantfile that will spin up a local Windows Server 2019 VM in Virtualbox.
After creating the VM it will automatically run our role.

### Development requirements

`pip3 install pywinrm`

#### Usage

- Run `vagrant up` to create a VM and run our role.
- Run `vagrant provision` to reapply our role.
- Run `vagrant destroy -f && vagrant up` to recreate the VM and run our role.
- Run `vagrant destroy` to remove the VM.

## License

MIT

## Authors

- John Potter, DREAM Team.  Developed from the work of Justin Perdok ([@justin-p](https://github.com/justin-p/))
