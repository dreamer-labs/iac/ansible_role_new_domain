pytest-testinfra
molecule>=3.2.2
openstacksdk<0.53
git+https://github.com/dingus9/ansible-coverage-callback.git@master#egg=ansible_coverage_callback
python-openstackclient==5.4.0
pywinrm
